import java.text.DecimalFormat;

public class ComplexNumber {
  private double real;
  private double imaginary;

  public ComplexNumber() {
    this.real = 0;
    this.imaginary = 0;
  }

  public ComplexNumber(double real, double imaginary) {
    this.real = real;
    this.imaginary = imaginary;
  }

  public String toString() {
    DecimalFormat d = new DecimalFormat("#0.000");
    return d.format(real) + "+" + d.format(imaginary) + "i";
  }

  public ComplexNumber plus(ComplexNumber other) {
    return new ComplexNumber(this.real + other.real, this.imaginary + other.imaginary);
  }

  public ComplexNumber times(ComplexNumber other) {
    double r = this.real * other.real + this.imaginary * -other.imaginary;
    double i = this.imaginary * other.real + this.real * other.imaginary;
    return new ComplexNumber(r, i);
  }

  public double getModulus() {
    return Math.sqrt(real * real + imaginary * imaginary);
  }

  public double getReal(){
    return real;
  }

  public void setReal(double real) {
    this.real = real;
  }

  public double getImaginary() {
    return imaginary;
  }

  public void setImaginary(double imaginary) {
    this.imaginary = imaginary;
  }

}
