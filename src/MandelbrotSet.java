import java.util.ArrayList;

public class MandelbrotSet {

	private ArrayList<ComplexNumber> set;

	public MandelbrotSet() {
		set = new ArrayList<ComplexNumber>();
		generateSet(-2.0, 1.0, 0.03, -1.0, 1.0, 0.03);
	}

	public MandelbrotSet(double lowR, double highR, double rInterval, double lowI, double highI, double iInterval) {
		set = new ArrayList<ComplexNumber>();
		generateSet(lowR, highR, rInterval, lowI, highI, iInterval);
	}

	public ArrayList<ComplexNumber> getSet() {
		return set;
	}

	public boolean isInSet(ComplexNumber c) {
		ComplexNumber z0 = new ComplexNumber();
		ComplexNumber z1 = new ComplexNumber();
		int maxIterations = 500;
		int iterations = 0;

		while (iterations < maxIterations) {
			z1 = z0.times(z0).plus(c);
			z0 = z1;
		    iterations++;
		}

		if (z1.getModulus() <= 2) {
			return true;
		} else { 
			return false;
		}
	}

	public void generateSet(double lowR, double hiR, double rInterval, double lowI, double hiI, double iInterval) {
		for (double real = lowR; real <= hiR; real += rInterval) {
			for (double imaginary = lowI; imaginary <= hiI; imaginary += iInterval) {
				ComplexNumber c = new ComplexNumber(real, imaginary);
				if(isInSet(c)) {
					set.add(c);
				}
			}
		}
	}
}
