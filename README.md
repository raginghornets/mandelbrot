# Mandelbrot

## Description
Mandelbrot in Processing.

## Controls
`z` - Increase resolution  
`x` - Decrease resolution  
`w` or mouse click - Zoom in  
`s` - Zoom out  
`q` - Quit  
`u` - Change color to black  
`i` - Change color to red  
`o` - Change color to green  
`p` - Change color to blue  
`←` `↑` `→` `↓` - Pan camera
